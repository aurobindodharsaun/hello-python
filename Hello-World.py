<<<<<<< HEAD
# This is essentially me learning Python's alphabets
# Obligatory "Hello World!" string inside print statement to kick off things
# Also used a comment to get two birds with one stone. Ha! How neat is that!

print("Hello you there! A new journey begins today")
print('Interesting')

# Didn't expect single quotes to work, but there we go

#Now some basic algebra with the tried and tested pair of "x" and "y"

x=8
y=2
print(x+y)
print(x-y)
print(x*y)
print(x/y)

# Turns out Python's print statement is way smarter than C/C++
# This right here is still an integer
print(8)

# However, this is interpreted as a string
print("8")

# Moving onto the venerable variables which are versatile and reusable
# Case in point

# string variable
nick_name="Silent Guardian"
print(nick_name)

nick_name="Watchful Protector"
print(nick_name)

nick_name="Watchful Protector 91939"
print(nick_name)

# integer variable
nick_name=91939
print(nick_name)

# Data types using Python type() function
type("I'm the Night!")

type(91939)

type(9.1939)

my_name="Silent Guardian"
type(my_name)

# Addition of numbers and strings
# Adding numbers

print(8+91931)

# Adding strings
print("I am " + "the Night")

# Adding variables
first_name = "Silent "
second_name = "Guardian"
print(first_name+second_name)

# Adding a variable and a string/number

# This works only if the variable is the same type as the string/number being added (str + str) and (int/float+int/float)
# Here is string+variable string
nick_name = "Silent Guardian"
"My friends call me " + nick_name

# Here is integer+variable float
TheDate=0.1939
print(9+TheDate)

# Intuitive knowledge of how to avoid SyntaxError and NameError gained
# I cannot think of a better way to demonstrate my newfound competence in python than the transcendental Lenny shrug face
print("¯\_(ツ)_/¯")

# Time to ask the lazy user on the other side of the screen to do some actual typing
# Time for a quick math trick
print("Enter an integer: ")
user_input = input()
print("The number you chose was: ", user_input)
print(type(user_input))
# Ha! Gotcha!!

# Now even more user input

user_name = input("Enter your name: ")
user_age = input ("How old are you: ")
print("Your name is " , user_name , " and you are " , user_age , " years old")

# Boolean string tests

user_name = input("Enter your name: ")
print (user_name.isalpha(), user_name.isalnum(), user_name.startswith("A"), user_name.isdecimal(), user_name.isdigit())
# String formatting
print (user_name.capitalize(), user_name.lower(), user_name.upper(), user_name.swapcase(), user_name.title())
# Alternatively, if you can make up your mind beforehand
print(input("Enter your name: ").upper())

# Using the in keyword to return a Boolean
# the in keyword can be used as a simple search returning True or False indication if a string is included in a target sequence.
exhort = "I am the Night"
print("'Night' in exhort = ",'Night' in exhort)
print("'night' in exhort = ", 'night' in exhort)
print("'night' in exhort if lower used = ", 'night'.lower() in exhort.lower())

# Searching a recipe in a menu
menu = "automatic, manual, semi-automatic, autonomous"
print('automatic' in menu)

# Mixing user input to make this even more elaborate by even adding the option of extending the list
mode = "automatic, manual, semi-automatic"
print("The operation modes available are: ", mode)
new_item=input("Enter items to add to the menu: ").lower()
new_mode=mode+ ", " + new_item
print("The new list of operation modes is: ", new_mode)
print(input("Enter the mode you want to select: ").lower() in new_mode.lower())

# Defining my first function
# Time to test user's knowledge of the Justice League
# The below function just removes duplicate entries from a large list to make it convenient for me
def remove_rejects(league_member):
    justice = []
    members = set()
    for leaguer in league_member:
        if leaguer not in members:
            justice.append(leaguer)
            members.add(leaguer)
    return justice

# Now, time to start the DC vs Marvel war

league_member = ["Superman","Batman","Wonder Woman","The Flash","Green Lantern","Aquaman","Martian Manhunter","Green Arrow","Atom","Hawkman","Black Canary","Phantom Stranger","Elongated Man","Red Tornado","Hawkgirl","Zatanna","Firestorm","Steel","Vixen","Vibe","Gypsy","Blue Beetle","Captain Marvel","Doctor Fate","Mister Miracle","Doctor Light","Booster Gold","Captain Atom","Rocket Red 7","Rocket Red 4","Fire","Ice","Hawkwoman","Huntress","Lightray","Orion","General Glory","Tasmanian Devil","Maxima","Ray","Black Condor","Agent Liberty","Bloodwynd","Flash","Justice League Europe","Animal Man","Metamorpho","Power Girl","Crimson Fox","Blue Jay","Silver Sorceress","Maya","Major Disaster","G'nort","Multi-Man","Big Sir","Cluemaster","Clock King","The Mighty Bruce","Scarlet Skier","Triumph","Nuklon","Obsidian","Amazing Man","Blue Devil","Icemaiden","L-Ron/Despero","Mystek","Zan & Jayna","Tomorrow Woman","Aztek","Oracle","Plastic Man","Zauriel","Big Barda","Hourman","Jade","Antaeus","Jesse Quick","Dark Flash","Moon Maiden","Nightwing","Faith","Etrigan","Manitou Raven","Sister Superior","Menagerie","Coldcast","Naif al-Sheikh","Kasumi","Manitou Dawn","Firehawk","Super-Chief","Bulleteer","Ambush Bug","Black Lightning","Red Arrow","Geo-Force","Supergirl","Starman","Congorilla","Guardian","Mon-El","Donna Troy","Cyborg","Starfire","Rocket Red #7","Snapper Carr","Sargon the Sorcerer","Golden Eagle","Captain Comet","Deadman","Sandman","Sue Dibny","Adam Strange","Tempest","Batgirl","Creeper","Retro","Resurrection Man","Toyman","Tattooed Man","Simon Carr","Dale Gunn","Maxwell Lord","Oberon","Catherine Cobert","Kilowog","L-Ron","Hannibal Martin","Yazz","The Atom","Element Woman","Shazam","Lex Luthor","Captain Cold","Mera","Madame Xanadu","Constantine","Shade  the Changing Man","Mind Warp","Andrew Bennett","Black Orchid","Doctor Mist","Timothy Hunter","Frankenstein","Amethyst","Swamp Thing","Nightmare Nurse","Pandora","The Phantom Stranger","August General in Iron","Godiva","Rocket Red","Batwing","O.M.A.C.","Olympian","Col. Steve Trevor","Katana","Stargirl","Catwoman","Equinox","Lobo","Killer Frost","Super-Man","Wonder-Woman","Bat-Man","Aquagirl","Warhawk","Micron"]
marvel_avenger = ["Iron Man","Thor","Ant-Man","Goliath","Yellowjacket","The Wasp","Ultron","Wasp","Hulk","Captain America","Hawkeye","Ronin","Quicksilver","Scarlet Witch","Swordsman","Hercules","Black Panther","Vision","Black Knight","Character","Black Widow","Mantis","Beast","Moondragon","Hellcat","Two-Gun Kid","Ms. Marvel","Warbird","Captain Marvel","Falcon","Wonder Man","Tigra","She-Hulk","Pulsar","Spectrum","Starfox","Namor","Doctor Druid","Doctor Droom","Mockingbird","War Machine","Iron Patriot","Thing","Moon Knight","Firebird","Demolition Man","Forgotten One","Mister Fantastic","Invisible Woman","U.S. Agent","Quasar","Human Torch","Sersi","Stingray","Spider-Man","Sandman","Rage","Machine Man","Living Lightning","Spider-Woman","Madame Web","Crystal","Thunderstrike","Darkhawk","Justice","Firestar","Triathlon","Silverclaw","Jack of Hearts","Captain Britain","Luke Cage","Wolverine","Sentry","Echo","Ares","Amadeus Cho","Jocasta","Stature","Winter Soldier","Valkyrie","Sharon Carter","Nova","Black Ant","Iron Fist","Power Woman","Jewel","Knightress","Protector","Doctor Strange","Red Hulk","Daredevil","Storm","Quake","Agent Venom","Havok","Sunspot","Cannonball","Manifold","Shang-Chi","Captain Universe","Smasher","Hyperion","Rogue","Sunfire","Doombot","Victor Mancha","Abyss","Ex Nihilo","Nightmask","Star Brand","Alexis The Protector","Blue Marvel","Power Man","White Tiger","Kaluu","Doctor Voodoo","Sabretooth","Validator","Pod","Deadpool","Synapse","Songbird","Squirrel Girl","Wiccan","Hulkling","Cable","Medusa","Singularity","Dazzler","Sister Grimm","Red Wolf","Flatman","Big Bertha","Doorman","Mister Immortal","Avenger X","Good Boy","Nightshade","Wheels Wolinski","Voyager","Ghost Rider","Rick Jones","Whizzer","Moira Brandon","Shroud","Marrina","Magdalene","Deathcry","Masque","Flux","Yondu","Martinex","Charlie-27","Nikki","Starhawk","Aleta Ogord","Vance Astro","Speed","Finesse","Hazmat","Mettle","Reptil","Striker","Lightspeed","X-23","Ravonna","Nick Fury","Dum Dum Dugan","Dominic Fortune","Namora","Kraven the Hunter","Ulysses Bloodstone","Silver Sable","Blonde Phantom"]
justice_league = remove_rejects(league_member)

def justice_league(real_name, superpower):
    print("\n"+super_hero+"'s secret identity is", real_name,"and is known as the",superpower)
    return
super_hero=input("Name a Member of the Justice League: ")
dc_comics=super_hero in league_member
marvel_comics=super_hero in marvel_avenger

if dc_comics:
    real_name=input("\nSecret Identity: ")
    superpower=input("\nAlso Known as ")
    hero_roster = justice_league(real_name, superpower)
    justice_league
elif marvel_comics:
    print("\n\n Go home Marvel fanboy! This game is for legends")
else:
    print("\n\nYou clearly have NO idea what you're talking about")

# Nested Conditionals, Escape Sequences and while loops

=======
# This is essentially me learning Python's alphabets
# Obligatory "Hello World!" string inside print statement to kick off things
# Also used a comment to get two birds with one stone. Ha! How neat is that!

print("Hello you there! A new journey begins today")
print('Interesting')

# Didn't expect single quotes to work, but there we go

#Now some basic algebra with the tried and tested pair of "x" and "y"

x=8
y=2
print(x+y)
print(x-y)
print(x*y)
print(x/y)

# Turns out Python's print statement is way smarter than C/C++
# This right here is still an integer
print(8)

# However, this is interpreted as a string
print("8")

# Moving onto the venerable variables which are versatile and reusable
# Case in point

# string variable
nick_name="Silent Guardian"
print(nick_name)

nick_name="Watchful Protector"
print(nick_name)

nick_name="Watchful Protector 91939"
print(nick_name)

# integer variable
nick_name=91939
print(nick_name)

# Data types using Python type() function
type("I'm the Night!")

type(91939)

type(9.1939)

my_name="Silent Guardian"
type(my_name)

# Addition of numbers and strings
# Adding numbers

print(8+91931)

# Adding strings
print("I am " + "the Night")

# Adding variables
first_name = "Silent "
second_name = "Guardian"
print(first_name+second_name)

# Adding a variable and a string/number

# This works only if the variable is the same type as the string/number being added (str + str) and (int/float+int/float)
# Here is string+variable string
nick_name = "Silent Guardian"
"My friends call me " + nick_name

# Here is integer+variable float
TheDate=0.1939
print(9+TheDate)

# Intuitive knowledge of how to avoid SyntaxError and NameError gained
# I cannot think of a better way to demonstrate my newfound competence in python than the transcendental Lenny shrug face
print("¯\_(ツ)_/¯")

# Time to ask the lazy user on the other side of the screen to do some actual typing
# Time for a quick math trick
print("Enter an integer: ")
user_input = input()
print("The number you chose was: ", user_input)
print(type(user_input))
# Ha! Gotcha!!

# Now even more user input

user_name = input("Enter your name: ")
user_age = input ("How old are you: ")
print("Your name is " , user_name , " and you are " , user_age , " years old")

# Boolean string tests

user_name = input("Enter your name: ")
print (user_name.isalpha(), user_name.isalnum(), user_name.startswith("A"), user_name.isdecimal(), user_name.isdigit())
# String formatting
print (user_name.capitalize(), user_name.lower(), user_name.upper(), user_name.swapcase(), user_name.title())
# Alternatively, if you can make up your mind beforehand
print(input("Enter your name: ").upper())

# Using the in keyword to return a Boolean
# the in keyword can be used as a simple search returning True or False indication if a string is included in a target sequence.
exhort = "I am the Night"
print("'Night' in exhort = ",'Night' in exhort)
print("'night' in exhort = ", 'night' in exhort)
print("'night' in exhort if lower used = ", 'night'.lower() in exhort.lower())

# Searching a recipe in a menu
modes = "automatic, manual, semi-automatic, autonomous"
print('automatic' in modes)

# Mixing user input to make this even more elaborate by even adding the option of extending the list
mode = "automatic, manual, semi-automatic"
print("The operation modes available are: ", mode)
new_item=input("Enter items to add to the menu: ").lower()
new_mode=mode+ ", " + new_item
print("The new list of operation modes is: ", new_mode)
print(input("Enter the mode you want to select: ").lower() in new_mode.lower())

# Defining and calling Functions with parameters
# Objects such as variables and functions aren't available until they have been processed as Python code is executed from top to bottom

def bat_nicknames(nickname1, nickname2):
    nick_name = ("the " + nickname1.title() + ", and the " + nickname2.title())
    return nick_name
bat_who=input("Who is he: ").title()
nickname1=input("\nOther known monikers: ")
nickname2=input("\nand: ")
bat_names = bat_nicknames(nickname1, nickname2)
print("\n"+bat_who+"\n\nAlso known as:", bat_names)















>>>>>>> 572a4179c139bf7ccf00530ac7fa6fa4f342199b
