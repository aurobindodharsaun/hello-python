# hello-python
This repository was an initiative to help my journey into Python development.

## Getting started with Python 3.x
  - Website: https://www.python.org
  - Documentation: https://docs.python.org
  - Developer's Guide: https://devguide.python.org/
  
## Python Development Environment

I used PyCharm community edition as the local IDE with `Python 3.6.4` as the virtualenv interpreter.

To get a local copy of the repository, I also setup GitHub for Windows and cloned a copy of this repository locally and synced it on my cloud storage account.

> The cloud storage sync part was all Windows 10's magic sauce. Neat!

### For enhanced portability, we will also be using Jupyter notebooks based on the Microsoft Azure Cloud Platform

(Note: Significant backward incompatible changes were made in Python 3.x, which may cause programs written for Python 2.x to fail when run with Python 3.x)

# License
This project is licensed under the `gpl-3.0` - see the [LICENSE.md](https://github.com/aurobindodharsaun/hello-python/blob/master/LICENSE.md) file for details



# What I did

Getting started is easy, with the new `hello-world` guide for GitHub which can be found here:

https://guides.github.com/activities/hello-world/

Other useful guides for step-by-step mastery of `GitHub`

https://guides.github.com/
